const getFunListInfo = require('./callback2')
const getFunCardInfo = require('./callback3')

const boardData = require('../boards.json')
const listInfo = getFunListInfo.getListData
const cardInfo = getFunCardInfo.getCardData

function problem4() {
    return new Promise((resolve, reject) => {
        setTimeout(async () => {
            let boardId = boardData.find(el => el.name === "Thanos")
            const listData = await listInfo(boardId.id)
            const listId = listData.filter(listId => (listId.name === "Mind" || listId.name === "Space"))
            let allCardsInfo = listId.map(async (el) => {
                    return await cardInfo(el.id)
                })
            resolveCardsInfo = await Promise.all(allCardsInfo)
            resolve(resolveCardsInfo)
        }, 2000)
    })
}

module.exports = {
    problem4
}
