/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    1. Get information from the Thanos boards
    2. Get all the lists for the Thanos board
    3. Get all cards for the Mind and Space lists simultaneously
*/

const getFun = require('../callback5')

const testData = [
  [
    {
      id: '1',
      description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
    },
    {
      id: '2',
      description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
    },
    {
      id: '3',
      description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
    }
  ],
  [
    {
      id: '1',
      description: 'intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.'
    },
    {
      id: '2',
      description: 'intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.'
    },
    {
      id: '3',
      description: 'intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.'
    },
    {
      id: '4',
      description: 'intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.'
    }
  ]
]

async function getResult() {
  const outputData = await getFun.problem4()
  if (JSON.stringify(outputData) === JSON.stringify(testData)) {
    console.log("Test Passed!")
  } else {
    console.log("Test Failed!")
  }
}

getResult()