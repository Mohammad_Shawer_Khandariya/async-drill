const getFun = require('../callback1')
const getData = getFun.getBoardData

const testData = [{
    "id": "abc122dc",
    "name": "Darkseid"
}]

async function getResult(id) {
    const outputData = await getData(id)
    if (JSON.stringify(outputData) === JSON.stringify(testData)) {
        console.log("Test Passed!")
    } else {
        console.log("Test Failed!")
    }
}

getResult("abc122dc")