/*
    Problem 1: Using callbacks and the fs module's asynchronous functions, do the following:
            1. Create a directory of random JSON files
            2. Delete those files simultaneously 
*/

const fs = require('fs').promises

function makeDir(dirName){
    return fs.mkdir(dirName, {recursive: true})
}

function createFile(fileName) {
    return fs.writeFile(fileName, '')
}

async function problem1(dir) {
    await makeDir(dir)
    for (let index = 0; index < 5; index++) {
        let randomFile = `${index}.json`
        let filePath = dir + randomFile
        await createFile(filePath)
    }
    const fileNames = await fs.readdir(dir)
    // console.log(fileNames)
    fileNames.forEach((file) => {
        let filePath = `${dir}${file}`
        fs.unlink(filePath, (err) => {
            if (err) {
                console.log(err)
            }
        })    
    })
}

module.exports = {
    problem1
}

/*
function makeDir(dirName) {
    return(new Promise((resolve, reject) => {
        fs.mkdir(dirName, {recursive: true}, (err) => {
            if (err) {
                reject(err)
            }
            resolve(dirName + '/')
        })
    }))
}

function randomFile() {
    return(Math.floor((Math.random() * 5) + 1).toString() + '.json')
}

function createFile(dirName, callBack) {
    const fileName = dirName + callBack()
    return(new Promise((resolve, reject) => {
        fs.writeFile(fileName, '', (err) => {
            if (err) {
                reject(error)
            }
            resolve(fileName)
        })
    }))
}

function deleteFile(fileName) {
    return(new Promise((resolve, reject) => {
        fs.unlink(fileName, (err) => {
            if (err) {
                console.log(err)
            }
            resolve(`Deleted successfully: ${fileName}`)
        })
    }))
}

async function problem1(dirName) {
    const dir = await makeDir(dirName)
    for (let index = 0; index < 5; index++) {
        const file = await createFile(dir, randomFile)
        await deleteFile(file)
    }
    
}
*/
/*
fs.mkdir('tempdir', {recursive: true}, (err) => {

    if (err) {

        console.log(err)

    }

    for (let index = 1; index <= 5; index++) {

        let fileName = 'tempdir/' + index + '.json'

        fs.writeFile(fileName, "", (err) => {

            if(err) {

                console.error()

            }

            console.log(`Created file: ${fileName}`)

            fs.unlink(fileName, (err) => {

                if (err) {

                    console.error();

                }

                console.log(`Deleted file: ${fileName}`)

            })

        })

    }

})
*/