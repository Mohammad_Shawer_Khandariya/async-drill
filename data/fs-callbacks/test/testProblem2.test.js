const getFun = require('../problem2')

const testData = "upperCase.txt lowerCase.txt sort.txt "

const fileReading = getFun.fileReading
const fileWriting = getFun.fileWriting
const fileUpdating = getFun.fileUpdating
const fileDeleting = getFun.fileDeleting

async function readWriteFile() {
    const content1 = await fileReading('lipsum.txt')
    const name1 = await fileWriting('upperCase.txt', content1.toUpperCase())
    fileUpdating(name1)
    const content2 = await fileReading('upperCase.txt')
    const name2 = await fileWriting('lowerCase.txt', JSON.stringify(content2.toLowerCase().split('\n')))
    fileUpdating(name2)
    const content3 = await fileReading('lowerCase.txt')
    const name3 = await fileWriting('sort.txt', JSON.stringify(JSON.parse(content3).sort()))
    fileUpdating(name3)
    const content4 = await fileReading('filenames.txt')
    await fileDeleting(content4.split(" "))
    if (testData === content4) {
        console.log("Test Passed!")
    } else {
        console.log("Test Failed!")
    }
}

readWriteFile()