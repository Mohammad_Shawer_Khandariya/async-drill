/* 
Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

const boardData = require('../boards.json')

function getBoardData(id) {
    return new Promise ((resolve) => {
        setTimeout(() => {
            // Your code here
            resolve(boardData.filter(el => el.id === id))
        }, 2 * 1000);
    })
}

module.exports = {
    getBoardData
}


/*
async function getResult(id) {
    const result = await getData()
    console.log(JSON.parse(result).filter(el => el.id === id) )
}

getResult("mcu453ed")

Using sync function
function readBoard(fileName) {
    return(
        fs.readFileSync(fileName, 'utf8', (err, data) => {
            if (err) {
                console.log(error)
            }
            return(data)
        })
    )
}

function getResult(fileName, key) {
    setTimeout((fileName) => {
        // Your code here
        fs.readFileSync(fileName, 'utf8', (err, data) => {
            if (err) {
                console.log(error)
            }
            return(data)
        }, 2 * 1000);
    })
}

function getResult(fileName, key, callBack) {
    const data = callBack(fileName)
    return(JSON.parse(data).filter((el) => (el.id === key)))
}

// Using async function

function readBoard(fileName) {
    return(new Promise((resolve, reject) => {
        fs.readFile(fileName, 'utf8', (err, data) => {
            if (err) {
                reject(error)
            }
            resolve(data)
        })
    }))

}

async function getResult(fileName, key, callBack) {
    try {
        const data = await callBack(fileName)
        console.log(JSON.parse(data).filter((el) => (el.id === key)))
    } catch (err) {
        console.log(err)
    }
    
}

getResult('../boards.json', 'mcu453ed', readBoard)

const fs = require('fs')

function getData(fileName) {
    return setTimeout((fileName) => {
            fs.readFile(fileName, 'utf8', (err, data) => {
                if (err) {
                    reject(err)
                } else{
                    resolve(data)
                }
            })
        }, 2000)
    }

async function getResult(fileName, id) {
    const result = getData(fileName)
    console.log(JSON.parse(result))
}

getResult('../boards.json', 'mcu453ed')

*/