const getFun = require('../callback3')
const readData = getFun.getCardData

const testData = [
    {
        "id": "1",
        "description": "Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar"
    },
    {
        "id": "2",
        "description": "Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar"
    },
    {
        "id": "3",
        "description": "Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar"
    }
]

async function getResult(id) {
    const outputData = await readData(id)
    if (JSON.stringify(outputData) === JSON.stringify(testData)) {
        console.log("Test Passed!")
    } else {
        console.log("Test Failed!")
    }
}

getResult("qwsa221")