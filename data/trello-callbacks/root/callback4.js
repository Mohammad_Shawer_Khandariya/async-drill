/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const boardData = require('../boards.json')
const getFunListInfo = require('./callback2')
const getFunCardInfo = require('./callback3')

const listInfo = getFunListInfo.getListData
const cardInfo = getFunCardInfo.getCardData

function problem4() {
    return new Promise((resolve, reject) => {
        setTimeout(async () => {
            const boardId = boardData.find(el => el.name === "Thanos")
            const listData = await listInfo(boardId.id)
            const listId = listData.find(listId => listId.name === "Mind")
            const cardData = await cardInfo(listId.id)
            resolve(cardData)
        }, 2000)
    })
}
module.exports = {
    problem4
}

/*
function readBoard(fileName) {
    return(
        fs.readFileSync(fileName, 'utf8', (err, data) => {
            if (err) {
                console.log(error)
            }
            return(data)
        })
    )
}

function getResult(fileName, key, callBack) {
    const data = callBack(fileName)
    let res = JSON.parse(data).filter((el) => (el.id === key))
    console.log(res)
}

getResult('../boards.json', "mcu453ed", readBoard)

function readLists(fileName) {
    return(fs.readFileSync(fileName, 'utf8', (err, data) => {
            if (err) {
                console.error(err)
            } else {
                return(data)
            }
        })
    )
}

function getLists(fileName, boardId, callBack) {
    const data = callBack(fileName)
    console.log(JSON.parse(data)[boardId])
}

getLists('../lists.json', "mcu453ed", readLists)

function readCards(fileName) {
    return(
        fs.readFileSync(fileName, 'utf8', (err, data) => {
            if (err) {
                console.log(error)
            }
            return(data)
        })
    )
}

function getResult(fileName, key, callBack) {
    const data = callBack(fileName)
    console.log(JSON.parse(data)[key])
}

getResult('../cards.json', 'qwsa221', readCards)
*/