const getFun = require('../callback4')

const testData = [
  {
    id: '1',
    description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
  },
  {
    id: '2',
    description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
  },
  {
    id: '3',
    description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
  }
]

async function getResult() {
  // Your code here
  const outputData = await getFun.problem4()
  if (JSON.stringify(outputData) === JSON.stringify(testData)) {
    console.log("Test Passed!")
  } else {
    console.log("Test Failed!")
  }
}

getResult()