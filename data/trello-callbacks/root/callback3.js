// Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.

const cardsData = require("../cards.json")

function getCardData(listsID) {
    return new Promise ((resolve) => {
        setTimeout(() => {
            // Your code here
            resolve(cardsData[listsID])
        }, 2 * 1000);
    })
}

module.exports = {
    getCardData
}

/*
function readCards(fileName) {
    return(
        fs.readFileSync(fileName, 'utf8', (err, data) => {
            if (err) {
                console.log(error)
            }
            return(data)
        })
    )
}

function getResult(fileName, key, callBack) {
    const data = callBack(fileName)
    console.log(JSON.parse(data)[key])
}

getResult('../cards.json', 'qwsa221', readCards)
*/