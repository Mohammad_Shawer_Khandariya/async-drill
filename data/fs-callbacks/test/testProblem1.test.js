const fs = require('fs')
const getFun = require('../problem1')

async function test(){
    const dir = 'newDir/'
    await getFun.problem1(dir)
    fs.access(dir, (err) => {
        if (err) {
            console.log("Test Failed: Directory does not exists.")
        } else {
            console.log("Test Passed: Directory exists.")
        }
    })
}

test()