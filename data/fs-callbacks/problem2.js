/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require('fs')

function fileReading(fileName) {
    return(new Promise ((resolve, reject) => {
        fs.readFile(fileName, 'utf8', (error, data) => {
            if (error) {
                reject(error)
            }
            resolve(data)
        })
    }))
}

function fileWriting(fileName, data) {
    return(new Promise((resolve, reject) => {
        fs.writeFile(fileName, data, (error) => {
            if (error) {
                reject(error)
            }
            resolve(fileName)
        })
    }))
}

function fileUpdating(fileName) {
    fs.appendFile('filenames.txt', fileName + " ", (error) => {
        if (error) {
            console.log(error)
        }
    })
}

// function deleteFile(fileName) {
//     return(new Promise((resolve, reject) => {
//         fs.unlink(fileName, (error) => {
//             if (error) {
//                 reject(`Unable to delete file ${fileName}`)
//             }
//             resolve(`File deleted: ${fileName}`)
//         })
//     }))
// }

async function fileDeleting(files) {
    return new Promise((resolve, reject) => {
        try {
            for (let index = 0; index < files.length; index++) {
                // let fileDeleted = await deleteFile(files[index])
                let fileName = files[index]
                fs.unlink(fileName, (error) => {
                    if (error) {
                        reject(`Unable to delete file ${fileName}`)
                    }
                    resolve(`File deleted: ${fileName}`)
                })
            }
        } catch(err) {
            console.log(err)
        }
    })
}

module.exports = {
    fileReading,
    fileWriting,
    fileUpdating,
    fileDeleting
}