// Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.

const listData = require('../lists.json')

function getListData(boardId) {
    return new Promise ((resolve, reject) => {
        setTimeout(() => {
            // Your code here
            resolve(listData[boardId])
        }, 2 * 1000);
    })
}

module.exports = {
    getListData
}


/*
function readLists(fileName) {
    return(fs.readFileSync(fileName, 'utf8', (err, data) => {
            if (err) {
                console.error(err)
            } else {
                return(data)
            }
        })
    )
}

function getLists(fileName, boardId, callBack) {
    const data = callBack(fileName)
    console.log(JSON.parse(data)[boardId])
}

getLists('../lists.json', "mcu453ed", readLists)
*/