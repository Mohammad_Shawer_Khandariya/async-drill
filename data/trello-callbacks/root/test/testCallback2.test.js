const getFun = require('../callback2')
const readData = getFun.getListData

const testData = [
    {
        "id": "xyzb778",
        "name": "Flight"
    },
    {
        "id": "llkj123",
        "name": "Telepathy"
    },
    {
        "id": "ghyu556",
        "name": "Telekinesis"
    }
]

async function getResult(boardId) {
    const outputData = await readData(boardId)
    if (JSON.stringify(outputData) === JSON.stringify(testData)) {
        console.log("Test Passed!")
    } else {
        console.log("Test Failed!")
    }
}

getResult('abc122dc')