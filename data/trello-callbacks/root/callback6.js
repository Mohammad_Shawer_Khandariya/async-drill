const getFunListInfo = require('./callback2')
const getFunCardInfo = require('./callback3')

const boardData = require('../boards.json')
const cardData = require('../cards.json')
const listInfo = getFunListInfo.getListData
const cardInfo = getFunCardInfo.getCardData

function problem6() {
    return new Promise((resolve, reject) => {
        setTimeout(async () => {
            let boardId = boardData.find(el => el.name === "Thanos")
            let listId = await listInfo(boardId.id)
            const allCardsInfo = listId.filter((el) => cardData.hasOwnProperty(el.id)).map(async (el) => await cardInfo(el.id))
            const resolveCardsInfo = Promise.all(allCardsInfo)
            resolve(resolveCardsInfo)
        }, 2000)
    })
}

module.exports = {
    problem6
}